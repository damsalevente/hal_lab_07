#include <iostream>

class Weapon{
    private:
        unsigned int damage;
    public:
        Weapon(int dmg = 10)
        {
            if(validate_input(dmg))
            {
                damage = dmg;
            }
       }
        virtual void toString();

        unsigned int get_damage()
        {
            return damage;
        }
        void set_damage(unsigned int dmg)
        {
            if(validate_input(dmg))
            {
                damage = dmg;
            }
        }
        virtual unsigned int use() 
        {
            return --damage;
        }
        bool validate_input(unsigned int dmg)
        {
            if(dmg<0 || dmg > 100)
            {
                return false;
            }
            return true;
        }
        // generate getter setter 
}

class Knife: Weapon{
   private:
        double sharpness;
   public:
        Knife(unsigned int dmg=30, double sharp=0.8):Weapon(dmg) // set default value  
        {
            sharpness = sharp;
        }
        void sharpen()
        {
            sharpness += (0.1%1);
        }
        unsigned int use()
        {
            return sharpness * this->get_damage();
        }
};
class Pistol: Weapon{
    unsigned clipSize;
    unsigned bulletsInClip;
    unsigned totalBullets;
    public:
        void reload();
        Pistol(int dmg=60,int _clipsize=12,int _bulletsinclip=12,int _totalbullets=48):Weapon(dmg) // is should call weapon default constructor, then extend with it's own shit 
        {
            clipSize = _clipsize;
            bulletsInClip = _bulletsinclip;
            totalBullets = _totalbullets;
    
        }
        void override use();

}
class Railgun : Weapon{
    unsigned energy;
    public:
        void recharge();
        Railgun();
        void use();
}
int main()
{
    Weapon a = new Weapon();
    a.toString();
    a.use();

    return 0;
}

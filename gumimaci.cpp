#include <iostream>
#include <string>
class Partner{
    private:
        char privateKey[2001];
        long id;
        Partner();

    public:
        Partner(long _id, char _privateKey[])
        {
            privateKey = _privateKey;
            id = _id;
        }
};
class Private : Partner{
    private:
        std::string firstname;
        std::string lastname;
    public:
        Private(long _id, char _privateKey[], std::string _firstname, std::string _lastname):Partner(_id, _privateKey)
        {
            firstname = _firstname;
            lastname = _lastname;
        }
};
class Business : Partner{
    private:
        std::string businessName;
        std::string taxNumber;
        std::string VATIN;
    public:
        Partner(long _id, char _privateKey[], std::string _businessName, std::string _taxNumber, std::string _VATIN): Partner(_id, _privateKey)
    {
        businessName = _businessName;
        taxNumber = _taxNumber;
        VATIN = _VATIN;
    }
};
int main()
{
    return 0;
}
